# Install greenplum role
Role just install GP.

After that you need enter in master, and init cluster.

## Use
### Install grenplum on all hosts
```bash
# Clone repo
git clone https://gitlab.com/kapuza-ansible/greenplum.git

# Enter to example dir
cd ./greenplum/example/

# Edit enventory
vim ./inventory/hosts

# Edit variables
vim ./roles/greenplum/defaults/main.yml

# Edit ansible config
vim ./ansible.cfg

# Run check
ansible-playbook --check install_greenplum.yml

# Run install
ansible-playbook install_greenplum.yml
```

### Initialization
```bash
# Login on server
ssh user@master-server

# Up to gpadmin
sudo -iu gpadmin

# Create hosts file
cat <<EOF > hostfile_gpinitsystem
gp-21
gp-22
gp-23
gp-24
EOF

# Copy and edit config
cp $GPHOME/docs/cli_help/gpconfigs/gpinitsystem_config ./gpinitsystem_config
vim ./gpinitsystem_config

# Init system
gpinitsystem -c gpinitsystem_config -h hostfile_gpinitsystem

# Add mster in profile
echo "export MASTER_DATA_DIRECTORY=/data/master/gpseg-1" >> .profile

# Cheeck install versiom
psql postgres -c "select version()"
```

